const getDurationFormatted = (duration) => {
  let durationHours = Math.floor(duration / 60);
  let durationMinutes = duration % 60;
  let durationFormatted =
    durationHours.toString().padStart(2, "0") +
    ":" +
    durationMinutes.toString().padStart(2, "0");
  return durationFormatted;
};

export default getDurationFormatted;

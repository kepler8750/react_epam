const formatCourseDate = (date) => {
  const day = date.getDate();
  const month = date.getMonth() + 1; // Добавляем 1, так как месяцы в JS начинаются с 0
  const year = date.getFullYear();

  return `${day}/${month}/${year}`;
};

export { formatCourseDate };

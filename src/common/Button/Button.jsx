import React from "react";
import css from "./Button.module.css";

export const Button = ({ text, onClick, type }) => (
  <button type={type} onClick={onClick} className={`${css.default} btn`}>
    {text}
  </button>
);

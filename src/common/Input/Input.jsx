import React from "react";

function Input({ type, placeholderText, onChange, onBlur, descr }) {
  return (
    <label>
      {descr && <div>{descr}</div>}
      <input
        type={type}
        placeholder={placeholderText}
        onChange={onChange}
        onBlur={onBlur}
      />
    </label>
  );
}

export default Input;

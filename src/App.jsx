import css from "./App.module.css";
import Header from "./components/Header/Header";
import Courses from "./components/Courses/Courses";
import { useState } from "react";
import CreateCourse from "./components/CreateCourse/CreateCourse";
import { mockedAuthorsList, mockedCoursesList } from "./constants";
import { formatCourseDate } from "./helpers/formatCourseDate";
import generateRandomId from "./helpers/generateRandomId";

function App() {
  const [createCourse, setCreateCourse] = useState(false);
  const [stateAuthorsList, setStateAuthorsList] = useState([
    ...mockedAuthorsList,
  ]);
  const [stateCoursesList, setStateCoursesList] = useState([
    ...mockedCoursesList,
  ]);
  const [newCourse, setNewCourse] = useState({
    id: false,
    title: false,
    description: false,
    creationDate: false,
    duration: false,
    authors: false,
  });

  const createCourseClick = (addCourse, addedAuthors) => {
    addCourse.authors = addedAuthors.map((author) => author.id);

    function mergeAuthors(authors, newCourseAuthors) {
      const authorIds = authors.map((author) => author.id);

      for (const author of newCourseAuthors) {
        if (!authorIds.includes(author.id)) {
          authors.push(author);
        }
      }

      return authors;
    }

    addCourse.creationDate = formatCourseDate(new Date());
    addCourse.id = generateRandomId();
    setStateAuthorsList(mergeAuthors(stateAuthorsList, addedAuthors));
    setStateCoursesList([...stateCoursesList, addCourse]);
    setNewCourse({
      id: false,
      title: false,
      description: false,
      creationDate: false,
      duration: false,
      authors: false,
    });
    setCreateCourse(false);
  };

  return (
    <div className={css.wraper}>
      <Header />
      {createCourse ? (
        <CreateCourse
          newCourse={newCourse}
          createCourseClick={createCourseClick}
          stateAuthorsList={stateAuthorsList}
        />
      ) : (
        <Courses
          setCreateCourse={setCreateCourse}
          stateAuthorsList={stateAuthorsList}
          stateCoursesList={stateCoursesList}
        />
      )}
    </div>
  );
}
export default App;

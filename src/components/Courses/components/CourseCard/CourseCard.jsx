import React from "react";
import { Button } from "../../../../common/Button/Button.jsx";
import { mockedAuthorsList } from "../../../../constants.js";

import css from "./CourseCard.module.css";
import getDurationFormatted from "../../../../helpers/getDurationFormated.js";

function CourseCard({ course, stateAuthorsList }) {
  const { title, description, creationDate, duration, authors } = course;
  let authorsList = [];

  return (
    <>
      <article className={css.wrap}>
        <div className={css.info}>
          <h2>{title}</h2>
          <div>{description}</div>
        </div>
        <div className={css.aside}>
          <div className={css.asideItem}>
            <strong>Authors: </strong>
            {authors.map((authorId) => {
              return authorsList.push(
                stateAuthorsList.find((item) => item.id === authorId)
              );
            })}
            {authorsList.map((item) => item.name).join(", ")}
          </div>
          <div className={css.asideItem}>
            <strong>Duration: </strong>
            {getDurationFormatted(duration)} hours
          </div>
          <div className={css.asideItem}>
            <strong>Created: </strong>
            {creationDate.replace(new RegExp("/", "g"), ".")}
          </div>
          <Button text={"Show course"} />
        </div>
      </article>
    </>
  );
}

export default CourseCard;

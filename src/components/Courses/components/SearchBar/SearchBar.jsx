import React, { forwardRef } from "react";
import { Button } from "../../../../common/Button/Button";
//import Input from "../../../../common/Input/Input";

const SearchBar = forwardRef((props, ref) => {
  return (
    <div>
      <input type={"search"} placeholder={"Enter course name..."} ref={ref} />
      <Button
        text={"Search"}
        onClick={(e) => {
          props.setFilterCourses(ref.current.value);
        }}
      />
    </div>
  );
});

export default SearchBar;

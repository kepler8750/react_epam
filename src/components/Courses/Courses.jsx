import React, { useState, useRef } from "react";
import CourseCard from "./components/CourseCard/CourseCard";
import { Button } from "../../common/Button/Button";
import SearchBar from "./components/SearchBar/SearchBar";

function Courses({ setCreateCourse, stateAuthorsList, stateCoursesList }) {
  const [filterCourses, setFilterCourses] = useState("");
  const searchInputRef = useRef(null);

  function getSortedCourses() {
    if (!filterCourses || !searchInputRef.current.value.length) {
      return stateCoursesList.map((course) => {
        return (
          <CourseCard
            key={course.id}
            course={course}
            stateAuthorsList={stateAuthorsList}
          />
        );
      });
    } else {
      return stateCoursesList
        .filter(
          (course) =>
            course.id
              .toLowerCase()
              .toString()
              .includes(filterCourses.toLowerCase()) ||
            course.title.toLowerCase().includes(filterCourses.toLowerCase())
        )
        .map((course) => {
          return (
            <CourseCard
              key={course.id}
              course={course}
              stateAuthorsList={stateAuthorsList}
            />
          );
        });
    }
  }

  return (
    <div>
      <SearchBar setFilterCourses={setFilterCourses} ref={searchInputRef} />
      <Button
        type="button"
        text={"Add new course"}
        onClick={() => {
          setCreateCourse(true);
        }}
      />

      {getSortedCourses()}
    </div>
  );
}

export default Courses;

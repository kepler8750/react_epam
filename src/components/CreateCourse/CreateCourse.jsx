import React, { useState, useRef } from "react";
import css from "./CreateCourse.module.css";
import { Button } from "../../common/Button/Button";
import Input from "../../common/Input/Input";
import getDurationFormatted from "../../helpers/getDurationFormated";
import generateRandomId from "../../helpers/generateRandomId";

function CreateCourse({ createCourseClick, stateAuthorsList, newCourse }) {
  const [authorsList, setAuthorsList] = useState([...stateAuthorsList]);
  let [courseAuthors, setCourseAuthors] = useState([]);
  let [courseDuration, setCourseDuration] = useState("");

  const authorNameRef = useRef();

  return (
    <div className={css.createCourseWrap}>
      <div className={css.createCourseBtnWrap}>
        <Input
          placeholderText={"Enter title..."}
          descr={"Title"}
          type={"text"}
          onBlur={(e) => {
            newCourse.title = e.target.value;
          }}
        />
        <Button
          type={"submit"}
          text={"Create course"}
          onClick={() => {
            createCourseClick(newCourse, courseAuthors);
          }}
        />
      </div>
      <label>
        <div>Description</div>
        <textarea
          cols="30"
          rows="10"
          placeholder="Enter description"
          minLength="2"
          onBlur={(e) => {
            newCourse.description = e.target.value;
          }}
        />
      </label>
      <div className={css.authorsBlock}>
        <div className={css.courseInfoWrap}>
          <h3>Add author</h3>

          <input
            placeholder={"Enter author name..."}
            descr={"Author name"}
            type={"text"}
            minLength={2}
            ref={authorNameRef}
          />

          <Button
            text={"Create author"}
            type={"submit"}
            onClick={(e) => {
              setAuthorsList([
                ...authorsList,
                { id: generateRandomId(), name: authorNameRef.current.value },
              ]);
              authorNameRef.current.value = "";
            }}
          />
          <h3>Duration</h3>
          <Input
            placeholderText={"Enter duration in minutes..."}
            descr={"Duration"}
            type={"text"}
            onChange={(e) => {
              setCourseDuration(e.target.value.replace(/\D/g, ""));
            }}
            onBlur={(e) => {
              newCourse.duration = courseDuration;
            }}
          />
          <div>
            Duration:
            <span className={css.boldHours}>
              {getDurationFormatted(courseDuration)}
            </span>
            hours
          </div>
        </div>
        <div className={css.authorsInfoWrap}>
          <h3>Authors</h3>
          {authorsList.length ? (
            <ul className={css.authorsList}>
              {authorsList.map((author) => (
                <li key={author.id}>
                  <span className={css.authorsName}>{author.name}</span>
                  <Button
                    text={"Add author"}
                    type={"submit"}
                    onClick={(e) => {
                      //добавляем автора в список авторов курса
                      setCourseAuthors([
                        ...courseAuthors,
                        { id: author.id, name: author.name },
                      ]);
                      //удаляем автора из общего списка
                      for (let i = 0; i < authorsList.length; i++) {
                        if (authorsList[i].name === author.name) {
                          authorsList.splice(i, 1);
                          break;
                        }
                      }
                    }}
                  />
                </li>
              ))}
            </ul>
          ) : (
            <div>"All authors added to course"</div>
          )}
          <h3>Course authors</h3>
          {courseAuthors.length ? (
            <ul className={css.authorsList}>
              {courseAuthors.map((author) => (
                <li key={author.id}>
                  <span className={css.authorsName}>{author.name}</span>
                  <Button
                    text={"Delete author"}
                    type={"submit"}
                    onClick={(e) => {
                      setAuthorsList([
                        ...authorsList,
                        courseAuthors.find((item) => item.name === author.name),
                      ]);
                      for (let i = 0; i < courseAuthors.length; i++) {
                        if (courseAuthors[i].name === author.name) {
                          courseAuthors.splice(i, 1);
                          break;
                        }
                      }
                      setCourseAuthors(courseAuthors);
                    }}
                  />
                </li>
              ))}
            </ul>
          ) : (
            <div>"Author list is empty"</div>
          )}
        </div>
      </div>
    </div>
  );
}

export default CreateCourse;

import React from "react";
import { Logo } from "./components/Logo/Logo";
import { Button } from "../../common/Button/Button";
import css from "./Header.module.css";

function Header() {
  return (
    <header className={css.header}>
      <Logo />
      <Button text="Logout" /*onClick={}*/ />
      <div className={css.userName}>UserName</div>
    </header>
  );
}

export default Header;

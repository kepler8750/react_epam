import React from "react";

import css from "./Logo.module.css";
import logo from "../../../../assets/logo.svg";

export const Logo = () => <img className={css.logo} src={logo} alt="logo" />;
